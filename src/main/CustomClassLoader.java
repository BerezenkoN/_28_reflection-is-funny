package main;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * Created by user on 05.02.2017.
 */
public class CustomClassLoader extends ClassLoader {
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        Class<?> result = findLoadedClass(name);
        if (result == null) {
            try {
                result = getParent().loadClass(name);
                return result;
            } catch (Exception e) {
                return loadClassFromResources(name);
            }
        } else {
            return result;
        }
    }

    private Class<?> loadClassFromResources(String name) throws ClassNotFoundException {
        String classPath = name.replaceAll("\\.", "/") + ".class";
        byte[] bytecode = null;
        Path fullClassPath = Paths.get("resources/" + classPath);
        if (Files.exists(fullClassPath)) {
            try {
                bytecode = Files.readAllBytes(fullClassPath);
            } catch (IOException e) {
                throw new RuntimeException("Cannot read file " + fullClassPath);
            }
        } else {
            String javaPath = name.replaceAll("\\.", "/") + ".java";
            Path fullJavaPath = Paths.get("resources/" + javaPath);
            if (Files.exists(fullJavaPath)) {
                JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
                if (compiler.run(null, null, null, fullJavaPath.toFile().getAbsolutePath()) == 0) {
                    try {
                        bytecode = Files.readAllBytes(fullClassPath);
                    } catch (IOException e) {
                        throw new RuntimeException("Cannot read file " + fullClassPath);
                    }
                } else {
                    throw new RuntimeException("Cannot compile class: " + name);
                }
            } else {
                throw new ClassNotFoundException("File " + fullJavaPath + " not found");
            }
        }
        if (bytecode != null) {
            return defineClass(name, bytecode, 0, bytecode.length);
        } else {
            throw new ClassNotFoundException("Class " + name + " not found");
        }
    }

    public static void main(String[] args) {
        CustomClassLoader classLoader = new CustomClassLoader();
        Class<?> exerciseClass = null;
        try {
            exerciseClass = classLoader.loadClass("com.myclasses.Exercise");


            System.out.println("Публичные поля");
            Field[] fields = exerciseClass.getFields();

            for (Field s : fields) {

                System.out.println(s.toString());

            }

            separator();


            System.out.println("Все поля, кроме унаследованных");
            Field[] declaredFields = exerciseClass.getDeclaredFields();

            for (Field s : declaredFields) {
                System.out.println(s.toString());
            }

            separator();


            System.out.println("Публичные методы");
            Method[] methods = exerciseClass.getMethods();

            for (Method s : methods) {
                Class[] paramTypes = s.getParameterTypes();
                System.out.println(s.toString());
                printParameterTypes(paramTypes);
            }

            separator();


            System.out.println("Все методы, кроме унаследованных");
            Method[] declaredMethods = exerciseClass.getDeclaredMethods();

            for (Method s : declaredMethods) {
                System.out.println(s.toString());
            }

            separator();


            Constructor constructor = exerciseClass.getConstructor();

            Object object = constructor.newInstance();
            fields = object.getClass().getDeclaredFields();
            for (Field s : fields) {
                String fieldName = s.getName();
                Type fieldType = s.getType();
                if (fieldType.equals(String.class)) {
                    boolean oldAccessible = s.isAccessible();
                    s.setAccessible(true);
                    String fieldValue = (String) s.get(object);
                    s.setAccessible(oldAccessible);


                }
            }
            separator();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void separator() {
        System.out.println("------------------------\n");

    }

    private static void printParameterTypes(Class[] paramTypes) {
        if (paramTypes.length == 0) {
            System.out.println("<Empty params>");
        } else {
            for (Class paramType : paramTypes) {
                System.out.print(paramType.getName());
            }
            System.out.println();
        }
    }
}
